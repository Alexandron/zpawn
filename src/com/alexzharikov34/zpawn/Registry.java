/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alexzharikov34.zpawn;

import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Хранилище данных, основанное на принципах реестра Windows
 * @author alexz
 * 
 */
public class Registry {
    
    private Reader regSource;
    
    public void setRegSource(Reader in) {
        regSource = in;
    }
    
    /*public class RegistryPathOverflowException extends Exception {
        
      /*  public RegistryPathOverflowException(String path) {
            System.err.println("RegistryPathOverflowException: Max size of \"" + path + "");
        } */
        
    //} 
    
    public interface IRegistryLoader {
        Object load(Reader in);
    }
    
    private final HashMap<String, Object> Storage;
    
    private String prepareStr(String s) { 
        if (!s.isEmpty()) {
            if (s.charAt(0) == '/') {
                s = s.substring(1);
            }
            if (s.charAt(s.length() - 1) == '/') {
                s = s.substring(0, s.length() - 1);
            }
        }
        return s;
    }
    
    private ArrayList<String> toWords(String s) {
        s = prepareStr(s) + '/';
        ArrayList<String> list = new ArrayList<>();
        while (s.contains("/")) {
            list.add(s.substring(0, s.indexOf("/")));
            s = s.substring(s.indexOf("/") + 1);
        }
        return list;
    }
    
    private int levelCount(String path) {
        int result = 1;
        path = prepareStr(path);
        while (path.contains("/")) {
            path = path.substring(0, path.indexOf("/")) + path.substring(path.indexOf("/") + 1);
            result++;
        }
        return result;
    }
    
    public Registry() {
        Storage = new HashMap<>();
    }
    
    public Registry(Reader source) {
        Storage = new HashMap<>();
        regSource = source;
    }
    
    /**
     * Создаёт каталог
     * @param name Полный путь каталога
     */
    public void addDir(String name) { 
        String path = "";
        for (int i = 0; i < levelCount(name); i++) {
            HashMap<String, Object> dir = new HashMap<>();
            put(path, toWords(name).get(i), dir);
            path = path + "/" + toWords(name).get(i);
        }
    }
    /**
     * Создаёт папку в ветке
     * @param path Полный путь родительского каталога
     * @param name Имя папки
     */
    public void addDir(String path, String name) {
        put(path, name, new HashMap<>());
    }
    
    public void loadDir(String path, IRegistryLoader loader) {
        put("", path, loader.load(regSource));
    }
    
    /**
     * Получает данные из каталога
     * @param path Полный путь каталога
     * @param key Имя данных
     * @return Данные в виде {@link Object}
     */
    public Object get(String path, String key) {
        if (isExist(path, key)) {
            if (path.isEmpty()) {
                return Storage.get(key);
            } else {
                return get_(Storage, toWords(path), 0, key);
            }
        } else return null;
    }
    
    private Object get_(HashMap<String, Object> container, ArrayList<String> path, int index, String key) {
        if (index < path.size()) {
            container = (HashMap < String, Object >) container.get(path.get(index));
            index++;
            Object result = get_(container, path, index, key);
            return result;
        } else {
            return container.get(key);
        }  
    }
    
    /**
     * Кладёт данные в каталог
     * @param path Полный путь каталога
     * @param key Имя данных
     * @param data Данные любого типа
     */
    public void put(String path, String key, Object data) {
        if (!Storage.containsKey(key)) {
            if (path.isEmpty()) {
                Storage.put(key, data);
            } else {
                put_(Storage, toWords(path), 0, key, data);
            }
        }
    }
    
    private HashMap<String, Object> put_(HashMap<String, Object> container, ArrayList<String> path, int index, String key, Object data) {
        if (index < path.size()) {
            container = (HashMap < String, Object >) container.get(path.get(index));
            index++;
            HashMap<String, Object> dir = put_(container, path, index, key, data);
            return dir;
        } else {
            if (container.containsKey(key)) {
                return null;
            } else {
                container.put(key, data);
                return container;
            }
        }
    }
    
    /**
     * Проверяет существование элемента (или каталога)
     * @param path Полный путь родительского каталога
     * @param key Ключ данных
     * @return True или False в зависимости от существования искомого элемента (каталога)
     */
    public boolean isExist(String path, String key) {
        if (path.isEmpty()) {
            return Storage.containsKey(key);
        } else {
            return isExist_(Storage, toWords(path), 0, key);
        }
    }
    
    private boolean isExist_(HashMap<String, Object> container, ArrayList<String> path, int index, String key) {
        if (index < path.size()) {
            container = (HashMap < String, Object >) container.get(path.get(index));
            Boolean result = isExist_(container, path, ++index, key);
            return result;
        } else {
            return container.containsKey(key);
        }  
    }
    
}
