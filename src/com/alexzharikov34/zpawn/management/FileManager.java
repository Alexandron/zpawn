/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alexzharikov34.zpawn.management;

import com.alexzharikov34.zpawn.MainForm;
import com.alexzharikov34.zpawn.Page;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
/**
 *
 * @author alexz
 */
public class FileManager extends CustomManager {
    
    private final PageManager pageManager;

    public FileManager(PageManager pm) {
        super(pm.DataStore, pm.tpEditors);
        pageManager = pm;
    }
    
    private void addOpenedFileInfo(File file) {
        String saveIndex = ((Page) tpEditors.getComponentAt(tpEditors.getSelectedIndex())).saveIndex;
        String header = file.getName();
        DataStore.put("pages/" + saveIndex, "filePath", file.getAbsolutePath());
        DataStore.put("pages/" + saveIndex, "fileName", header);
        pageManager.updatePageHeader(tpEditors.getSelectedIndex(), header);
    }
    
    private class DialogResult {
        public int status;
        public File file;

        public DialogResult(int status, File file) {
            this.status = status;
            this.file = file;
        }
        
    }
    
    private DialogResult createOpenDialog() {
        return createDialog(JFileChooser.OPEN_DIALOG, "Open file");
    }
    
    private DialogResult createSaveDialog(String dTitle) {
        return createDialog(JFileChooser.SAVE_DIALOG, dTitle);
    }
    
    private DialogResult createDialog(int dType, String dTitle) {
        JFileChooser openChooser = new JFileChooser();
        openChooser.setDialogType(dType);
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Text file (*.txt)", "txt");
        openChooser.setAcceptAllFileFilterUsed(false);   // Необходимо для перемещения
        openChooser.addChoosableFileFilter(filter);      // пункта All Files
        openChooser.setAcceptAllFileFilterUsed(true);    // в конец списка расширений
        int ret = openChooser.showDialog(null, dTitle);
        return new DialogResult(ret, openChooser.getSelectedFile());
    }
    
    public void openFile() {
        DialogResult result = createOpenDialog();
        if (result.status == JFileChooser.APPROVE_OPTION) {
            pageManager.addPage();
            tpEditors.setSelectedIndex(tpEditors.getTabCount() - 1);
            openFile_(result.file);
        }
        pageManager.getCurrentEditor().setCaretPosition(0);
    }
    
    private void openFile_(File file)  {
        try {
                try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "windows-1251"))) {
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while((line = br.readLine()) != null) {
                        
                        sb.append(line).append("\n");
                    }
                    pageManager.getCurrentEditor().setText(sb.toString());
                    addOpenedFileInfo(file);
                }

        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private String currentPath() {
        String saveIndex = ((Page) tpEditors.getComponentAt(tpEditors.getSelectedIndex())).saveIndex;
        return (DataStore.get("pages/" + saveIndex, "filePath") != null) ? DataStore.get("pages/" + saveIndex, "filePath").toString() : "";
    }
    
    public void compile() throws IOException {
        final String COMPILIER_PATH = DataStore.get("settings", "compilier_path").toString();
        ProcessBuilder pb = new ProcessBuilder(COMPILIER_PATH, currentPath(),"-o=D:\\netproject\\ZPawn\\Caligula.amx");
        pb.start();
    }
    
    public void saveFileAs() {
        saveFileAs(false);
    }
    
    private void saveFileAs(Boolean firstSaving) {
        String dialogTitle = firstSaving ? "Save file" : "Save file as";
        DialogResult result = createSaveDialog(dialogTitle);
        if (result.status == JFileChooser.APPROVE_OPTION) {
            addOpenedFileInfo(result.file);
            saveFile();
        }
    }

    public void saveFile() {
        if (currentPath().isEmpty()) {
            saveFileAs(true);
        } else {
            try {
                try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(currentPath())), "windows-1251"))) {
                    bw.write(pageManager.getCurrentEditor().getText());
                }
            } catch (FileNotFoundException | UnsupportedEncodingException ex) {
                Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
     
}
