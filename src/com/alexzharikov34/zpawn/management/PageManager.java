/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alexzharikov34.zpawn.management;

import com.alexzharikov34.zpawn.Page;
import com.alexzharikov34.zpawn.Registry;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.MouseInfo;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rtextarea.RTextScrollPane;

/**
 *
 * @author alexz
 */
public class PageManager extends CustomManager {

    public PageManager(Registry DataStore, JTabbedPane tpEditors) {
        super(DataStore, tpEditors);
        
    }
    
    /** Формирует и обновляет заголовки вкладок при создании вкладки или открытии файла.
     *  @param index Номер вкладки
     *  @param text Заголовок вкладки
     **/
    public void updatePageHeader(int index, String text) {
        JPanel title = new JPanel();
        JButton tButton = new JButton("x");
        JLabel tLabel = new JLabel(text);
        tLabel.setFont(new Font("Segoe UI", 0, 15));
        title.add(tLabel); 
        tButton.setBorder(BorderFactory.createEmptyBorder(1, 10, 1, 1));
        tButton.setOpaque(false);
        tButton.setContentAreaFilled(false);
        tButton.setBorderPainted(false);
        tButton.setFont(new Font("Segoe UI", 1, 14));
        tButton.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                //tpEditors.remove(index);//To change body of generated methods, choose Tools | Templates.
                tpEditors.remove(tpEditors.getComponentAt(MouseInfo.getPointerInfo().getLocation()));
            }

            @Override
            public void mousePressed(MouseEvent e) {
                tButton.setForeground(Color.red); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                tButton.setForeground(Color.black); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                tButton.setText("X"); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void mouseExited(MouseEvent e) {
                tButton.setText("x"); //To change body of generated methods, choose Tools | Templates.
            }
        });
        title.add(tButton);
        title.setLayout(new GridBagLayout());
        title.setOpaque(false);
        tpEditors.setTabComponentAt(index, title);
    }
    
    public RSyntaxTextArea getCurrentEditor() {
        Component[] compList = ((JPanel) tpEditors.getComponentAt(tpEditors.getSelectedIndex())).getComponents();
                for(Component c : compList) {
                    if (c instanceof RTextScrollPane) {
                        return (RSyntaxTextArea) ((RTextScrollPane) c).getViewport().getView();
                    }
                }
        return null;
    }
    
    public void addPage() {
        Page page = new Page();
        Boolean reg = true;
        while (reg) { 
            if (((HashMap<String, Object>) DataStore.get("", "pages")).size() <= 14776336) {
                page.generateIndex();
                reg = DataStore.isExist("pages", page.saveIndex);
            } else {
                break;
            }
        }
        if (!reg) {
            DataStore.addDir("pages/" + page.saveIndex);
        }
        tpEditors.addTab("", page);
        updatePageHeader(tpEditors.getTabCount() - 1, "Untitled");
        
    }
    
}
