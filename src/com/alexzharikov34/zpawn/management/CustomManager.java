/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alexzharikov34.zpawn.management;

import com.alexzharikov34.zpawn.Registry;
import javax.swing.JTabbedPane;

/**
 *
 * @author alexz
 */
public abstract class CustomManager {
    
    protected final Registry DataStore;
    protected final JTabbedPane tpEditors;
    
    public CustomManager(Registry store, JTabbedPane editors) {
        DataStore = store;
        tpEditors = editors;
    }
    
}
